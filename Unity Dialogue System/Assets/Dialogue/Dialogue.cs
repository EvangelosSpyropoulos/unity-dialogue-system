﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue
{
    public Line[] lines;
}

[System.Serializable]
public class Line
{
    public string name;

    [TextArea(3, 10)]
    public string sentence;
}
