﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartConversation : MonoBehaviour
{
    [SerializeField] Conversation conversation = null;

    public void SpawnConversation()
    {
        Instantiate(conversation);
    }
}
