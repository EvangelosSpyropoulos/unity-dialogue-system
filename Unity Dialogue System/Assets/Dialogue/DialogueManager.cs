﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{

    [SerializeField] Text nameText = null;
    [SerializeField] Text dialogueText = null;

    [SerializeField] Image dialogueBox = null;

    Queue<string> names;
    Queue<string> sentences;

    // Start is called before the first frame update
    void Start()
    {
        names = new Queue<string>();
        sentences = new Queue<string>();
    }

    public void StartDialogue(Dialogue dialogue)
    {
        names.Clear();
        sentences.Clear();

        dialogueBox.gameObject.SetActive(true);

        foreach (Line line in dialogue.lines)
        {
            names.Enqueue(line.name);
            sentences.Enqueue(line.sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (names.Count == 0)
        {
            EndDialogue();
            return;
        }

        string name = names.Dequeue();
        string sentence = sentences.Dequeue();
        nameText.text = name;
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));

    }

    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    private void EndDialogue()
    {
        dialogueBox.gameObject.SetActive(false);
    }
}
