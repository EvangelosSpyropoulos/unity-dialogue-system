# Unity Dialogue System

Based on [Brackeys' monologue system](https://youtu.be/_nRzoTzeyxU), turned into a dialogue one and having decoupled the conversation architecture from the button, so instead of the conversations being stored in the button
turned into a file-based system.